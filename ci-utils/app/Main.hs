module Main where


import           Control.Monad                  ( void )
import           Data.Text                      ( Text
                                                , pack
                                                , unpack
                                                , concat
                                                )
import           Data.Maybe                     ( catMaybes )

import           Replace.Megaparsec
import           Text.Megaparsec
import           Options.Applicative
import           Data.Semigroup                 ( (<>) )
import           Lib


data Options = Options {
    fileName :: String,
    variableName :: VariableName,
    variableValue :: VariableValue
}



options :: Parser Options
options =
    Options
        <$> strOption
                (long "filename" <> metavar "FILENAME" <> help
                    "Filename where the variable to update exists"
                )
        <*> strOption
                (long "variable-name" <> metavar "VARIABLE NAME" <> help
                    "Variable name to update"
                )
        <*> strOption
                (long "variable-value" <> metavar "VARIABLE VALUE" <> help
                    "New variable value"
                )

opts = subparser
    (command
        "update-variable"
        (info
            (options <**> helper)
            (  fullDesc
            <> progDesc "CI utils to update variable"
            <> header "update-variable - Update a variable in a given file."
            )
        )
    )

updateVariable :: Options -> IO ()
updateVariable = undefined


operate :: Options -> IO ()
operate (Options fileName varName varValue) = do
    fileContent <- readFile fileName
    putStr
        (unpack
            (streamEdit
                (match $ pVar varName)
                (\(s, r) -> Data.Text.concat [variable r, pack (" = "  ++ varValue)])
                (pack fileContent)
            )
        )


main :: IO ()
main = operate =<< execParser (info opts idm)







