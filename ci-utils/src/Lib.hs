module Lib
    ( Assignment(..)
    , pVar
    , VariableName
    , VariableValue
    )
where


import           Data.Text                      ( Text )
import           Data.Void

import           Text.Megaparsec
import           Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer    as L


type Parser = Parsec Void Text

type Config = String
type VariableName = Text
type VariableValue = String

updateVariable :: Config -> VariableName -> Config
updateVariable = undefined


spaceConsumer :: Parser ()
spaceConsumer = L.space space1
                        (L.skipLineComment "//" <|> L.skipLineComment "#")
                        (L.skipBlockComment "/*" "*/")

variableNameConsumer :: VariableName -> Parser Text
variableNameConsumer = chunk

assignmentOperator :: Parser Char
assignmentOperator = single '='

stringLiteral :: Parser String
stringLiteral = char '\"' *> manyTill L.charLiteral (char '\"')

surroundedBy s = between s s

data Assignment = Assignment {
    variable :: Text,
    assignment :: Char
}

newVar :: Assignment -> VariableValue -> String
newVar = undefined

pVar :: VariableName -> Parser Assignment
pVar variableName = do
    variable   <- variableNameConsumer variableName
    assignment <- surroundedBy spaceConsumer assignmentOperator
    spaceConsumer
    stringLiteral
    return Assignment { variable = variable, assignment = assignment }
